    function  firebaseConfig() {
     var config = {
      	apiKey: "AIzaSyC2qTCQsgP6Nmp0xB-PqNvQygkQ668esLo",
	authDomain: "zrenix-aae2e.firebaseapp.com",
	databaseURL: "https://zrenix-aae2e.firebaseio.com",
	projectId: "zrenix-aae2e",
	storageBucket: "zrenix-aae2e.appspot.com",
	messagingSenderId: "1095365181062",
	appId: "1:1095365181062:web:db9b34e80b0572fc"
     };
     return config; 
    }

    //firebase.initializeApp(firebaseConfig());

function SignIn(O) { 
	var provider = new firebase.auth.GoogleAuthProvider();
  //firebase.auth().signInWithPopup(provider)
  firebase.auth().signInWithRedirect(provider).then(function() {
  return firebase.auth().getRedirectResult();
  }).then(function(result) { }).catch(function(error) { 
	  alert('Error!'); 
	  console.log(error);
  });


}

function SignOut(O) {
  firebase.auth().signOut().then(function(results) { 
    $('#SignOutIMG').html(''); $('#SignIn').show(); $('#SignOut').hide(); 
  }).catch(function(error) { alert('Error!'); }); 
}

function initApp() {
  firebase.auth().onAuthStateChanged(function(user) {
    if (user) { var name = user.displayName, email = user.email, emailVerified = user.emailVerified;
      var n = name + '(' + email + ')';
      $('#SignOutIMG').html('<img width=40px height=40px title=\''+n+'\' src=\''+user.photoURL+'\' />');
      $('#SignIn').hide(); $('#SignOut').show(); 
      //var uid = user.uid, phoneNumber = user.phoneNumber, providerData = user.providerData;
    } else { $('#SignIn').show(); $('#SignOut').hide(); }
  }, function(error) { console.log(error); });
};
//window.addEventListener('load', function() { initApp(); });
